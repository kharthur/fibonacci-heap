public class Node {
    public Node child, parent, left, right;
    public int key, degree;
    public boolean mark;

    public Node(int element) {
        this.child = null;
        this.parent = null;
        this.right = this;
        this.left = this;
        this.key = element;
        this.degree = 0;
        this.mark = false;
    }
}