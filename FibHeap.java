class FibHeap {
    public Node min;
    public int count;

    public FibHeap() {
        this.min = null;
        this.count = 0;
    }

    public boolean isEmpty() {
        return min == null;
    }

    public void clear() {
        this.min = null;
        this.count = 0;
    }

    public void insert(int element) {
        Node node = new Node(element);
        node.key = element;
        if (count == 0) {
            min = node;
        } else {
            Node oldRight = min.right;
            min.right = node;
            node.left = min;
            node.right = oldRight;
            oldRight.left = node;
        }
        if(node.key < min.key){
            min = node;
        }
        count++;
    }

    public int minKey(){
        return min.key;
    }

    public void union(Node a, Node b){
        Node L = a.left;
        Node R = b.right;
        b.right = a;
        a.left = b;
        L.right = R;
        R.left = L;
    }

    public void merge(FibHeap heap){
        if(heap.count == 0){
            return;
        }
        if(this.count == 0){
            this.min = heap.min;
            this.count = heap.count;
        }else{
            union(this.min, heap.min);
            count += heap.count;
        }
        if(heap.min.key < this.min.key){
            min = heap.min;
        }
    }

    public void consolidate(){
        Node[] A = new Node[count];
        A[min.degree] = min;
        Node current = min.right;
        while(A[current.degree] != current){
            if(A[current.degree] == null){
                A[current.degree] = current;
                current = current.right;
            }else{
                Node conflict = A[current.degree];
                Node addTo, adding;
                if(conflict.key < current.key){
                    addTo = conflict;
                    adding = current;
                }else{
                    addTo = current;
                    adding = conflict;
                }
                union(addTo.child, adding);
                adding.parent = addTo;
                addTo.degree++;
                current = addTo;
            }
            if(current.key < min.key){
                min = current;
            }
        }
    }

    public int deleteMin(){
        Node prevMin = min;
        union(min, min.child);
        if(prevMin.right == prevMin){
            min = null;
        }else{
            min = min.right;
            consolidate();
        }
        count--;
        return prevMin.key;
    }

    public void cut(Node x){
        x.parent.degree--;
        if(x.parent.child == x){
            if(x.right == x){
                x.parent.child = null;
            }else{
                x.parent.child = x.right;
            }
        }
        x.right = x;
        x.left = x;
        x.mark = false;
        x.parent = null;
        union(min,x);
    }

    public void cascadingCut(Node x){
        while(x.mark == true){
            cut(x);
            x = x.parent;
        }
        x.mark = true;
    }

    public void decreaseKey(Node x, int newVal){
        if(newVal > x.parent.key){
            x.key = newVal;
            return;
        }
        Node parent = x.parent;
        cut(x);
        cascadingCut(parent);
    }
}    